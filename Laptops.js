
export const mock_laptops = [
    {   
        id: 1,
        name : "Lenovo Legion Y540",
        brand : "Lenovo",
        price : 500,
        description: "Tag din gamingpassion med dig på farten. Lenovo Legion Y540 tilbyder fremragende ydelse med præcist bagoplyst tastatur, sofistikeret afkøling, intelligent placerede porte, kraftfulde Harman højttalere og masser af kraft.",
        image : "https://www.elgiganten.dk/image/dv_web_D180001002449285/150634/lenovo-legion-y540-156-baerbar-gamingcomputer-sort.jpg?$fullsize$"

    },

    {   
        id: 2,
        name : "Macbook Pro 16” Premium edition",
        brand : "Mac",
        price : "One Kidney",
        description: "MacBook Pro 16 hjælper dig med at klare arbejdet hurtigt og effektivt med dens 8-core Intel Core processor og Radeon Pro grafik. Den har et præcist Magic Keyboard for en smidig tasteoplevelse, opgraderede højttalere og mikrofoner i studiekvalitet.",
        image : "https://www.elgiganten.dk/image/dv_web_D180001002533639/224556/macbook-pro-16-premium-edition-space-gray.jpg?$fullsize$"

    },

    {   
        id: 3,
        name : "Surface Pro 6",
        brand : "Microsoft",
        price : 700,
        description: "Opdag den slanke, stærkere og hurtigere Surface Pro 6. Kraften fra en 8. gen. Intel Core i5 processor er parret med tablet-lignende bærbarhed, skarp 12,3” PixelSense skærm og heldags batterilevetid frigør dit potentiale, uanset hvor du tager hen.",
        image : "https://www.elgiganten.dk/image/dv_web_D180001002231495/18159/surface-pro-6-128-gb-i5-platin.jpg?$fullsize$"

    },

    {   
        id: 4,
        name : "HP EliteBook 840 G7",
        brand : "HP",
        price : 1100,
        description: "Den bærbare HP EliteBook 840 G7 14\" computer tilbyder høj ydelse i et kompakt design. Det hårdføre aluminiumskabinet er ideelt til brug på farten, og de mange sikkerhedsfunktioner og den lange batteritid gør enheden velegnet til lange arbejdsdage.",
        image : "https://www.elgiganten.dk/image/dv_web_D180001002483044/190199/hp-elitebook-840-g7-14-baerbar-computer-soelv.jpg?$fullsize$"

    },

]
