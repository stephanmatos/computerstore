import{mock_laptops} from "./Laptops.js" 

// Objects
const bank = new Bank();
const work = new Work();

// Text Fields

// Name
const ELName = document.getElementById("nameOfPerson");
// Money balance
const ELBankBalance = document.getElementById("moneyBalance");
// Pay balance
const ELPayBalance = document.getElementById("payBalance");
// Loan balance Text
const ELTextLoanBalance = document.getElementById("loanText");
// Loan balance Number
const ELLoanBalance = document.getElementById("loanBalance");
// Laptop drop down
const ELLaptopDropDown = document.getElementById("laptopDropdown");
// Laptop Title Text
const ELLaptopTitleText = document.getElementById("titleText");
// Laptop Description Text 
const ELLaptopDescriptionText = document.getElementById("discriptionText");
// Laptop Price Text
const ELLaptopPriceText = document.getElementById("priceText");


// Buttons 

// Get Loan
const ELLoanButton = document.getElementById("obtainLoanButton");

// Repay loan
const ELRepayButton = document.getElementById("repayLoanButton");

// Transfer from work to money account
const ELTransferButton = document.getElementById("transferButton");

// Work 
const ELWorkButton = document.getElementById("workButton");

// Buy a computer
const ELBuyButton = document.getElementById("buyButton");

// Image
const ELLaptopImage = document.getElementById("imageContainer");

// Loan button functionality
ELLoanButton.addEventListener("click",function(){
    if (bank.loanBalance > 0) {
        // Show message loan exist
        showAlert("You can not have two loans");
        return;
    }
    openLoanPromt("Please desired loan amount");
});

// Open loan prompt for user input
function openLoanPromt(message) {
    let desiredAmount = prompt(message, "200");

    // check for cancel
    if(desiredAmount == null){
        return;
    }

    // check for invalid characters
    if(isNaN(desiredAmount)){
        openLoanPromt("Illegal charater - please enter a number");
        return;
    }
    checkAmount(desiredAmount);
}

// check if the desired loan amount is within parameters
function checkAmount(amount) {
    if (bank.bankBalance * 2 < amount) {
        openLoanPromt("Desired amount to high - You can loan a maximum of " + bank.bankBalance*2);
        return;
    }
    bank.loanBalance = amount;
    bank.bankBalance += parseInt(amount);
    updateVisualVariables();
}

// Work button functionality
ELWorkButton.addEventListener("click",function(){
    work.work();
    updateVisualVariables(); 
});

// Transfer from work to money account 
ELTransferButton.addEventListener("click", function(){
    let pay = work.workBalance;
    work.resetPayBalance();
    bank.addBalance(pay);
    updateVisualVariables();
});

// Repay loan with work money functionality
ELRepayButton.addEventListener("click",function(){
    if(bank.loanBalance == 0){
        showAlert("You can not repay a non existing loan");
        return;
    }
    if(work.workBalance == 0){
        showAlert("You can not repay with 0 DKK");
        return;
    }

    let pay = work.workBalance;
    work.resetPayBalance();
    bank.repayLoan(pay); 
    updateVisualVariables();
})

// Dropdown menu functionality
ELLaptopDropDown.addEventListener("change",function(){

    let currLaptop = ELLaptopDropDown.selectedIndex;
    
    ELLaptopTitleText.innerText = mock_laptops[currLaptop].name;

    ELLaptopDescriptionText.innerText = mock_laptops[currLaptop].description;
    
    ELLaptopPriceText.innerText = mock_laptops[currLaptop].price;

    ELLaptopImage.src = mock_laptops[currLaptop].image;
})

// Buy button functionality
ELBuyButton.addEventListener("click",function(){
    let currLaptop = ELLaptopDropDown.selectedIndex;

    if(mock_laptops[currLaptop].price > bank.bankBalance){
        alert("You can not afford the computer");
        return;
    }

    if(mock_laptops[currLaptop].id == 2){
        if(window.confirm("Are you sure you want part with one kidney ? You can only do this once!")){
            
            alert("Rip")
        }else{
            alert("Good choice! Now go get a normal priced laptop")

        }
        return;
    }

    if(window.confirm("Are you sure you want to buy the computer?")){
        bank.bankBalance -= mock_laptops[currLaptop].price;
        updateVisualVariables();
        alert("Congratulations, you have bought a computer!")
    }
})

// Update values on screen
function updateVisualVariables() {

    // Update name
    ELName.innerText = "Joe Banker";

    // update money balance
    ELBankBalance.innerText = bank.bankBalance + " DKK";

    // update pay balance
    ELPayBalance.innerText = work.workBalance + " DKK";

    // update loan balance
    if (bank.loanBalance > 0) {
        ELTextLoanBalance.innerText = "Loan balance";
        ELLoanBalance.innerText = bank.loanBalance + " DKK";
    }else  {
        ELTextLoanBalance.innerText = "";
        ELLoanBalance.innerText =  "";
    }

    // Show hide button
    if(bank.loanBalance == 0){
        ELRepayButton.style.display = "none"
    }else{
        ELRepayButton.style.display = ""
    }

    let currLaptop = ELLaptopDropDown.selectedIndex
    
    ELLaptopTitleText.innerText = mock_laptops[currLaptop].name;

    ELLaptopDescriptionText.innerText = mock_laptops[currLaptop].description
    
    ELLaptopPriceText.innerText = mock_laptops[currLaptop].price

    ELLaptopImage.src = mock_laptops[currLaptop].image
}

// Fill the dropdown with laptop names
function laptopDropDown(){
   
    for(let i = 0 ; i < mock_laptops.length ; i++){
        const listItem = document.createElement("option")
        listItem.innerText = mock_laptops[i].name
        ELLaptopDropDown.appendChild(listItem)
    }

}

// Show alert with message
function showAlert(message){
    window.alert(message);
}

// Init dropdown and visual variables
laptopDropDown()
updateVisualVariables()

