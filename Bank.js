class Bank{

    // Create 
    constructor(){
        this.bankBalance = 0
        this.loanBalance = 0
    }

    // Ad the work balance to the bank balance. Take out 10% if a loan is taken
    addBalance(amount){
        if(this.loanBalance != 0){
            // Calculate 10% of salary
            let deduction = amount * 0.1
            if(this.loanBalance > deduction){
                // Loan is bigger than the 10% repayment 
                this.loanBalance -= deduction
    
            }else{
                // Loan is smaller than the 10% repayment
                deduction = this.loanBalance

                console.log(deduction)
                this.loanBalance = 0
            }
            amount -= deduction
            console.log(amount)
        }
        this.bankBalance += amount
    }

    // Subtract the amount from the work account from the loan balance
    repayLoan(amount){
        if(amount > this.loanBalance){
            this.bankBalance += (amount-this.loanBalance)
            this.loanBalance = 0
        }else{
            this.loanBalance -= amount
        }

    }

    // Subtract the price of the computer from bank balance
    buyComputer(price){
        if(price <= this.bankBalance){
            this.bankBalance -= price
        }
    }
}
 
